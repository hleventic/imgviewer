# File name: program.py
import sqlite3

from kivy.app import App
from kivy.uix.boxlayout import BoxLayout

class MyBoxLayout(BoxLayout):
    pass


class MyProgramApp(App):
    def build(self):
        return MyBoxLayout()

if __name__ == "__main__":
    conn = sqlite3.connect('test.db')
    c = conn.cursor()

    exts = [] 
    for row in c.execute("SELECT * from settings where key = 'extension' order by value asc"):
        exts.append(row[2])
    

    found_files= [] 
    for row in c.execute("SELECT * from settings where key = 'folder' order by value asc"):
        current_path = row[2]

        for root, dirs, files in os.walk(current_path, topdown=False):
            for name in files:
                for ext in exts: 
                    if name.endswith(ext):
                        found_files.append(name)

    # found_files sadrzi slike

    

    MyProgramApp().run()
